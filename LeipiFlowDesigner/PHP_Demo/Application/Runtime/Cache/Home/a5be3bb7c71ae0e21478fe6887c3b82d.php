<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
 <head>
    
    <title>流程设计器 实例演示 Flowdesign.leipi.org</title>
    <meta name="keyword" content="流程设计器实例演示,Web Flowdesign,Flowdesigner,专业流程设计器,WEB流程设计器">
    <meta name="description" content="流程设计器实例演示，国内最容易使用和开发的流程设计器，你可以在此基础上任意修改使功能无限强大！">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="author" content="leipi.org">
    <link href="/Public/css/bootstrap/css/bootstrap.css?<?php echo ($_cjv); ?>" rel="stylesheet" type="text/css" />
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="/Public/css/bootstrap/css/bootstrap-ie6.css?<?php echo ($_cjv); ?>">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="/Public/css/bootstrap/css/ie.css?<?php echo ($_cjv); ?>">
    <![endif]-->
    <link href="/Public/css/site.css?<?php echo ($_cjv); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var _root='<?php echo ($_site_url); echo U('/');?>',_controller = '<?php echo ($_controller); ?>';
    </script>
    

 </head>
<body>

<!-- fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="brand" href="http://www.leipi.org" target="_blank">雷劈网</a>
      <div class="nav-collapse collapse">
        <ul class="nav">
            <li><a  href="http://flowdesign.leipi.org/">流程设计器</a></li>
            <li><a href="http://formdesign.leipi.org">表单设计器</a></li>
            <li><a href="http://qrcode.leipi.org">自动生成二维码</a></li>
            <li><a href="http://flowdesign.leipi.org/index.php?s=/doc.html">文档</a></li>
			<li class="active"><a href="<?php echo U('/demo');?>">实例</a></li>
            <li><a href="http://flowdesign.leipi.org/index.php?s=/downloads.html">下载</a></li>
            <li><a href="http://flowdesign.leipi.org/index.php?s=/feedback.html">公开讨论</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>


	


<div class="bs-header" id="content">
  <div class="container">

    <h1>办理工作</h1>
    <p>
       填好表后请记得保存或转交给下一步经办人喔。
    </p>
    
  </div>
</div>

<div class="container">

<div class="row">
    <ol class="breadcrumb">
        <li><a href="/">流程设计器</a> <span class="divider">/</span></li>
        <li><a href="<?php echo U('/demo');?>">实例</a> <span class="divider">/</span></li>
        <li class="active"><?php if(empty($run_process)): ?>发起<?php else: ?>办理<?php endif; ?></li>
    </ol>
</div>

<div class="row">
<form action="<?php echo U('/run/edit_save');?>" method="post">
<input type="hidden" value="<?php echo ($run_process["id"]); ?>" name="run_process">

<p>
	<h4><i class="icon-play"></i>工作名称</h4>
    <input type="text" class="span6" placeholder="必填项" name="run_name" value="<?php echo ($run_one["run_name"]); ?>">
</p>
<hr/>
<p>
	<h4><i class="icon-play"></i>填写表单</h4>
    <?php echo ($design_content); ?>
</p>
<hr/>
<p>
	<h4><i class="icon-play"></i>上传附件</h4>
	未完成...
</p>
<hr/>
<p>
	<h4><i class="icon-play"></i>会签意见</h4>
	未完成...
</p>


<hr/>
<button type="submit" name="submit_to_save" value="save" class="btn btn-primary">确定保存</button>
<button type="submit" name="submit_to_next" value="next" class="btn btn-info">保存转交下一步</button>
<button type="submit" name="submit_to_end" value="end" class="btn btn-success">办结</button>
</form>


</div><!--end row-->
</div><!--end container-->




<block name="footer_js">

<!-- script end -->



    
    
 <div class="bs-footer" role="contentinfo" id="bs-footer">
      <div class="container">
          <p><span class="glyphicon glyphicon-list-alt"></span> 免责声明：本站仅分享开发思路和示例代码并且乐于交流和促进网络良性发展，是非商业工具，如有疑问请加群或邮件告知，积极配合调整。</p>
          <p><span class="glyphicon glyphicon-list-alt"></span> 反馈：payonesmile@qq.com、<a target="_blank" style="color:#f30" href="http://www.leipi.org/commit-code/"><i class="icon-leaf"></i>我要贡献示例</a></p>

          <p><img src="http://formdesign.leipi.org/Public/images/alipay.png" width="100" alt="支付宝扫一扫"  title="有你们的支持，我们将做的更好"/>
            支持：支付宝扫一扫，<a href="http://formdesign.leipi.org/support.html"  title="有你们的支持，我们将做的更好">查看记录</a></p>

          <p><span class="glyphicon glyphicon-bookmark"></span> 鸣谢：<a href="http://ueditor.baidu.com" target="_balnk">UEditor</a>、<a href="http://www.bootcss.com" target="_balnk">Bootstrap中文网</a>、<a href="http://www.leipi.org" target="_balnk">雷劈网</a>、
            <a href="http://git.leipi.org" target="_balnk">GitHub</a>、
            <a href="http://www.wwei.cn/" target="_balnk" title="二维码生成器">二维码生成器</a>

          </p>
          <p><a href="http://www.leipi.org" title="雷劈网"><img src="http://www.leipi.org/wp-content/themes/leipi/images/leipi.png" alt="雷劈认证 icon" style="height:30px"></a> &copy;2014 Ueditor Formdesign Plugins v4 leipi.org <a href="http://www.miitbeian.gov.cn/" target="_blank">粤ICP备13051130号</a></p>
      </div>
</div>
    <!--footer js -->
<div style="display: none;">                                                                   
88888888888  88                             ad88  88                ad88888ba   8888888888   
88           ""                            d8"    88               d8"     "88  88           
88                                         88     88               8P       88  88  ____     
88aaaaa      88  8b,dPPYba,   ,adPPYba,  MM88MMM  88  8b       d8  Y8,    ,d88  88a8PPPP8b,  
88"""""      88  88P'   "Y8  a8P_____88    88     88  `8b     d8'   "PPPPPP"88  PP"     `8b  
88           88  88          8PP"""""""    88     88   `8b   d8'            8P           d8  
88           88  88          "8b,   ,aa    88     88    `8b,d8'    8b,    a8P   Y8a     a8P  
88           88  88           `"Ybbd8"'    88     88      Y88'     `"Y8888P'     "Y88888P"   
                                                          d8'                                
2014-3-15 Firefly95、xinG                                 d8'  
</div>

<div style="display:none">
<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F1e6fd3a46a5046661159c6bf55aad1cf' type='text/javascript'%3E%3C/script%3E"));
</script>
</div>

</body>
</html>